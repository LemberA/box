FROM node

RUN mkdir /tipsitip
WORKDIR /tipsitip
COPY . /tipsitip

RUN yarn install
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000
